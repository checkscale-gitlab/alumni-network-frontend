import {
	Modal,
	Backdrop,
	Fade,
	Box,
	Typography,
	TextField,
	Grid,
	Button,
	MenuItem,
	Alert,
	Snackbar,
} from "@mui/material";
import React, { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { selectGroupIds, selectGroupById } from "../group/groupSlice";
import { selectTopicById, selectTopicIds } from "../topic/topicSlice";

interface editPostProps {
	openEditPost: boolean;
	handleCloseEditPost: (openPost: boolean) => void;
}

const EditPostModal = (props: editPostProps) => {
	const dispatch = useAppDispatch();

	// Local states
	const [editPostTitle, setEditPostTitle] = useState<string>("");
	const [editPostGroup, setEditPostGroup] = useState<string>("");
	const [editPostTopic, setEditPostTopic] = useState<string>("");
	const [editContent, setEditContent] = useState<string>("");
	const [openSnack, setOpenSnack] = useState(false);

	const groups = useAppSelector(selectGroupIds);
	const groupById = useAppSelector(selectGroupById);
	const topics = useAppSelector(selectTopicIds);
	const topicById = useAppSelector(selectTopicById);

	const handleEditPostTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setEditPostTitle(event.target.value);
	};

	const handleEditPostGroupChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setEditPostGroup(event.target.value);
	};

	const handleEditPostTopicChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setEditPostTopic(event.target.value);
	};

	const handleEditContentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setEditContent(event.target.value);
	};

	const cancelClick = () => {
		props.handleCloseEditPost(false);
	};

	const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
		if (reason === "clickaway") {
			return;
		}
		setOpenSnack(false);
	};

	const editPostClick = () => {
		/*const post: IPostBody = {
			title: editPostTitle
			postGroup: editPostGroup
			postMessage: editContent
		};*/
		// dispatch(createPost(post));
		// props.handleCloseEditPost(false);
		// setOpenSnack(true);
	};

	const style = {
		position: "absolute" as "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 600,
		bgcolor: "white",
		border: "2px solid #000",
		boxShadow: 24,
		p: 4,
	};

	return (
		<>
			<Modal
				aria-labelledby="transition-modal-title"
				aria-describedby="transition-modal-description"
				open={props.openEditPost}
				onClose={props.handleCloseEditPost}
				closeAfterTransition
				BackdropComponent={Backdrop}>
				<Fade in={props.openEditPost}>
					<Box sx={style}>
						<Typography
							id="transition-modal-title"
							variant="h6"
							component="h2"
							textAlign="center">
							Edit Post
						</Typography>
						<Typography sx={{ mt: 2 }}>
							<TextField
								margin="normal"
								fullWidth
								id="title"
								label="Title"
								name="Title"
								defaultValue={editPostTitle}
								onChange={handleEditPostTitleChange}
							/>
							<TextField
								select
								margin="normal"
								fullWidth
								name="group"
								label="Group"
								id="group"
								defaultValue={editPostGroup}
								onChange={handleEditPostGroupChange}>
								{groups.map((id) => (
									<MenuItem key={id} value={id}>
										{id}. {groupById[parseInt(id)].name}
									</MenuItem>
								))}
							</TextField>
							<TextField
								select
								margin="normal"
								fullWidth
								name="topic"
								label="Topic"
								id="topic"
								defaultValue={editPostTopic}
								onChange={handleEditPostTopicChange}>
								{topics.map((id) => (
									<MenuItem key={id} value={id}>
										{id}. {topicById[parseInt(id)].name}
									</MenuItem>
								))}
							</TextField>
							<TextField
								margin="normal"
								fullWidth
								name="postContent"
								label="Post Content"
								id="postContent"
								multiline
								rows={16}
								defaultValue={editContent}
								onChange={handleEditContentChange}
							/>
							<Grid container justifyContent="space-between">
								<Button
									color="error"
									variant="contained"
									sx={{ mt: 3, mb: 2 }}
									onClick={cancelClick}>
									Cancel
								</Button>
								<Button variant="contained" sx={{ mt: 3, mb: 2 }}>
									Save Changes
								</Button>
							</Grid>
						</Typography>
					</Box>
				</Fade>
			</Modal>
			<Snackbar open={openSnack} autoHideDuration={6000} onClose={handleClose}>
				<Alert onClose={handleClose} severity="success" sx={{ width: '100%', fontSize: 24, bgcolor: "lightgreen" }}>
					Post successfully Updated!
				</Alert>
			</Snackbar>
		</>
	);
};

export default EditPostModal;
