export interface ITopic {
	topicId: number;
	name: string;
	description: string;
	members?: number[];
	events?: number[];
	posts?: number[];
}

export interface ITopicById {
	[id: number]: ITopic;
}
