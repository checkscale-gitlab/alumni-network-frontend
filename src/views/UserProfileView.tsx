import { Typography } from "@mui/material";
import React from "react";
import { useAppSelector } from "../app/hooks";
import { selectCurrentUserId } from "../features/allUsers/usersSlice";
import UserProfile from "../features/user/UserProfile";
import withAuth from "../hoc/withAuth";

const UserProfileView = () => {
	const currentUserId = useAppSelector(selectCurrentUserId);
	return (
		<div>
			{currentUserId ? (
				<UserProfile></UserProfile>
			) : (
				<Typography>No user selected</Typography>
			)}
		</div>
	);
};

export default withAuth(UserProfileView);
