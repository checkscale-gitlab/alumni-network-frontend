import {
	Avatar,
	Divider,
	Grid,
	IconButton,
	Paper,
	Tooltip,
	Typography,
} from "@mui/material";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { convertDate } from "../../common/helperFunctions";
import { selectGroupById } from "../group/groupSlice";
import {
	selectPostById,
	selectPostsIds,
	setActivePostId,
} from "../post/postSlice";
import PostThreadItem from "./PostThreadItem";
import { useState } from "react";
import { createPost } from "../post/postSlice";
import { IPostBody } from "../../api/post/postApiTypes";
import { selectUserId } from "../user/userSlice";
import ReplyPost from "./ReplyPost";
import CloseIcon from "@mui/icons-material/Close";
import ArticleIcon from "@mui/icons-material/Article";

interface ThreadProps {
	id: string;
	handleUsernameClick: (userId: string) => void;
}

/**
 * The post thread component
 * @param props 
 * @returns the post thread component
 */
const PostThread = (props: ThreadProps) => {
	const dispatch = useAppDispatch();

	const [success, setSuccess] = useState(false);
	const [error, setError] = useState<string | undefined>();

	const postInfo = useAppSelector(
		(state) => state.posts.postById[parseInt(props.id)]
	);
	const owner = useAppSelector(
		(state) => state.allUsers.userById[postInfo.senderId]
	);
	const postIds = useAppSelector(selectPostsIds);
	const post = useAppSelector(selectPostById);
	const groupById = useAppSelector(selectGroupById);
	const loggedInUserId = useAppSelector(selectUserId);

	const usernameClick = () => {
		props.handleUsernameClick(owner.userId.toString());
	};

	const handleSendMessageClick = (e: any, message: string) => {
		setError(undefined);
		e.preventDefault();
		setSuccess(false);
		if (loggedInUserId !== 0 && postInfo) {
			const post: IPostBody = {
				lastUpdated: "",
				postMessage: message,
				title: postInfo.title,
				senderId: loggedInUserId,
				postTarget: {
					postId: postInfo.postId,
				},
			};
			console.log(post);
			dispatch(createPost(post))
				.unwrap()
				.then(() => {
					setSuccess(true);
				})
				.catch((error) => {
					console.log(error);
					setError(error.errorMessage);
				});
		}
	};

	const closeThread = () => {
		dispatch(setActivePostId(undefined));
	};

	return (
		<Grid item sx={{ mt: 9.5 }}>
			<Grid item>
				<Paper elevation={5} sx={{ padding: 2 }}>
					<Grid container justifyContent="space-between" alignItems="center">
						<Grid item>
							<Grid container alignItems="center" spacing={2}>
								<Grid item>
									<Grid container alignItems="center">
										<ArticleIcon sx={{ mr: 1 }} />
										<Typography component="h1" variant="h4">
											{postInfo.title}
										</Typography>
									</Grid>
								</Grid>
								<Grid item>
									<Typography variant="body2">
										#
										{postInfo.target_group
											? groupById[postInfo.target_group].name
											: "no group"}
									</Typography>
								</Grid>
							</Grid>
						</Grid>
						<Grid item>
							<IconButton onClick={closeThread}>
								<CloseIcon />
							</IconButton>
						</Grid>
					</Grid>

					<Divider sx={{ mb: 2 }} />
					<Grid
						container
						justifyContent="start"
						alignItems="center"
						spacing={2}>
						<Grid item>
							<Avatar src={owner.picture} alt="" />
						</Grid>
						<Grid item>
							<Grid container alignItems="center">
								<Tooltip placement="top" title={"go to " + owner.name + "'s profile"}>
									<Typography
										onClick={usernameClick}
										component="h1"
										variant="h5"
										sx={{
											mr: 2,
											padding: 0.5,
											borderRadius: 4,
											"&:hover": { cursor: "pointer", bgcolor: "lavender" },
										}}>
										{owner.name}
									</Typography>
								</Tooltip>
								<Typography>{convertDate(postInfo.lastUpdated)}</Typography>
							</Grid>
						</Grid>
					</Grid>
					<Grid item sx={{ ml: 7.5 }}>
						<Typography component="h1" variant="h6">
							{postInfo.postMessage}
						</Typography>
					</Grid>
					{postIds.map(
						(id: string) =>
							post[parseInt(id)].postTarget === parseInt(props.id) && (
								<PostThreadItem
									key={id}
									id={parseInt(id)}
									threadId={props.id}
									handleUsernameClick={props.handleUsernameClick}
								/>
							)
					)}
					<Grid container marginTop={2}>
						<ReplyPost
							handleSendMessageClick={handleSendMessageClick}
							success={success}
							error={error}
						/>
					</Grid>
				</Paper>
			</Grid>
		</Grid>
	);
};

export default PostThread;
