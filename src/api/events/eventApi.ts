import { createAuthenticatedHeader, createHeader } from "../helperFunctions";
import { IEventBody, IUpdateEvent } from "./eventTypes";

const apiUrl = process.env.REACT_APP_ALUMNI_NETWORK_URL;

/**
 * Fetches all events available for the user
 * @returns Events
 */
export const fetchEvents = async () => {
	try {
		const response = await fetch(`${apiUrl}/event`, {
			method: "GET",
			headers: createAuthenticatedHeader(),
		});
		if (!response.ok) throw new Error("Could not fetch events");

		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Creates a new event
 * @param event the information of the event
 * @returns The event created
 */
export const createEventRequest = async (event: IEventBody) => {
	try {
		const response = await fetch(`${apiUrl}/event`, {
			method: "POST",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify(event),
		});
		if (!response.ok) throw new Error("Could not create event");

		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

// Under are temporary endpoints request that were setup but not in use in the first version of the app.

export const updateEvent = async (req: IUpdateEvent) => {
	try {
		const response = await fetch(`${apiUrl}/event/${req.eventId}`, {
			method: "PUT",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify(req.body),
		});
		if (!response.ok) throw new Error("Could not update event");

		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

export const createEventGroupInvite = async (
	eventId: number,
	groupId: number
) => {
	try {
		const response = await fetch(
			`${apiUrl}/event/${eventId}/invite/group/${groupId}`,
			{
				method: "POST",
				headers: createAuthenticatedHeader(),
			}
		);
		if (!response.ok) throw new Error("Could not create group invite");

		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};

export const deleteEventGroupInvite = async (
	eventId: number,
	groupId: number
) => {
	try {
		const response = await fetch(
			`${apiUrl}/event/${eventId}/invite/group/${groupId}`,
			{
				method: "DELETE",
				headers: createAuthenticatedHeader(),
			}
		);
		if (!response.ok) throw new Error("Could not delete invite");

		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};

export const createEventTopicInvite = async (
	eventId: number,
	topicId: number
) => {
	try {
		const response = await fetch(
			`${apiUrl}/event/${eventId}/invite/topic/${topicId}`,
			{
				method: "POST",
				headers: createAuthenticatedHeader(),
			}
		);
		if (!response.ok) throw new Error("Could not create topic invite");

		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};

export const deleteEventTopicInvite = async (
	eventId: number,
	topicId: number
) => {
	try {
		const response = await fetch(
			`${apiUrl}/event/${eventId}/invite/topic/${topicId}`,
			{
				method: "DELETE",
				headers: createAuthenticatedHeader(),
			}
		);
		if (!response.ok) throw new Error("Could not delete invite");

		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};

export const createEventUserInvite = async (
	eventId: number,
	userId: number
) => {
	try {
		const response = await fetch(
			`${apiUrl}/event/${eventId}/invite/user/${userId}`,
			{
				method: "POST",
				headers: createAuthenticatedHeader(),
			}
		);
		if (!response.ok) throw new Error("Could not create event invite for user");

		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};

export const deleteEventUserInvite = async (
	eventId: number,
	userId: number
) => {
	try {
		const response = await fetch(
			`${apiUrl}/event/${eventId}/invite/user/${userId}`,
			{
				method: "DELETE",
				headers: createAuthenticatedHeader(),
			}
		);
		if (!response.ok) throw new Error("Could not delete event invite");

		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};
