export interface IPostsById {
	[id: number]: IPost;
}

export interface IPost {
	postId: number;
	lastUpdated: string;
	senderId: number;
	postMessage: string;
	title: string;
	target_topics?: number[];
	target_event?: number;
	target_group?: number;
	postTarget?: number;
	target_user?: number;
}
