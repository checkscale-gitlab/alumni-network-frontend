import { ListItem, ListItemText, Switch, Tooltip } from "@mui/material";
import React, { useState } from "react";
import { ITopicMembershipRequest } from "../../api/topic/topicTypes";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { NavbarListItemProps } from "../group/GroupListItem";
import { selectUserName } from "../login/loginSlice";
import {
	selectUserId,
	selectUserTopics,
	subscribeOrUnsubscribeToTopic,
} from "../user/userSlice";
import {
	selectActiveTopicId,
	setActiveTopicId,
	subscribeToTopicRequest,
	unsubscribeToTopic,
} from "./topicSlice";

/**
 * A topic list item
 * @param props
 * @returns a topic list item
 */
const TopicListItem = (props: NavbarListItemProps) => {
	const dispatch = useAppDispatch();

	const activeTopicId = useAppSelector(selectActiveTopicId);
	const topic = useAppSelector(
		(state) => state.topic.topicById[parseInt(props.id)]
	);
	const userTopics = useAppSelector(selectUserTopics);
	const isSubscribed = userTopics?.includes(parseInt(props.id));
	const loggedInUser = useAppSelector(selectUserId);
	const user = useAppSelector(selectUserName);

	const [checked, setChecked] = useState(isSubscribed ? true : false);

	const handleTopicItemClick = () => {
		dispatch(setActiveTopicId(props.id));
		props.handleClose && props.handleClose();
	};

	const handleSubscribeClick = () => {
		const req: ITopicMembershipRequest = {
			topicId: parseInt(props.id),
			userId: loggedInUser,
		};
		if (isSubscribed)
			dispatch(unsubscribeToTopic(req))
				.unwrap()
				.then(() => {
					dispatch(subscribeOrUnsubscribeToTopic(parseInt(props.id)));
					setChecked(false);
				});
		else
			dispatch(subscribeToTopicRequest(req))
				.unwrap()
				.then(() => {
					dispatch(subscribeOrUnsubscribeToTopic(parseInt(props.id)));
					setChecked(true);
				});
	};

	return (
		<ListItem disablePadding>
			<ListItemText primary={topic.name} />
			<Tooltip placement="right" title={checked ? "Unsubscribe" : "Subscribe"}>
				<Switch onClick={handleSubscribeClick} checked={checked} />
			</Tooltip>
		</ListItem>
	);
};

export default TopicListItem;
