import { RootState } from "./../app/store";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface SearchState {
	searchQuery: string;
}

const initialState: SearchState = {
	searchQuery: "",
};

/**
 * Part of the redux store that handles the searchQuery
 */
export const searchQuerySlice = createSlice({
	name: "search",
	initialState,
	reducers: {
		setTimelineSearchQuery: (state, action: PayloadAction<string>) => {
			state.searchQuery = action.payload;
		},
	},
});

export const { setTimelineSearchQuery } = searchQuerySlice.actions;

export const selectSearchQuery = (state: RootState) => state.search.searchQuery;

export default searchQuerySlice.reducer;
