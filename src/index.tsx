import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { store } from "./app/store";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";
import { Auth0Provider } from "@auth0/auth0-react";

const apiUrl = process.env.REACT_APP_ALUMNI_NETWORK_URL;
const audience = "https://alumni-network/auth";

ReactDOM.render(
	<React.StrictMode>
		<Auth0Provider
			domain="dev-c1vp-b3z.eu.auth0.com"
			clientId="AbDS1RyvstuSNZtJc7Lh130icaEL3yHq"
			redirectUri={window.location.origin}
			audience={audience}
			scope="read:messages">
			<Provider store={store}>
				<App />
			</Provider>
		</Auth0Provider>
	</React.StrictMode>,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
